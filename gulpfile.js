'use strict'

const path = require('path');
const gulp = require('gulp');
const $ = require('gulp-load-plugins')();
const fileinclude = require('gulp-file-include');
const combine = require('stream-combiner2');
const browserSync = require('browser-sync').create();
const rimraf = require('rimraf');

const ENV = {
	dev: $.environments.development,
	prod: $.environments.production
}

gulp.task('html', () => {
	let combined = combine.obj([
		gulp.src('./source-markup/*.html'),
		gulp.dest('./local/templates/assets/')
	]);

	combined.on('error', console.error.bind(console));
	return combined;
});

gulp.task('styles', () => {
	let combined = combine.obj([
		gulp.src('./source-markup/css/style.less'),
		ENV.dev($.sourcemaps.init()),
		$.less({
            relativeUrls: true
		}),
		$.autoprefixer({ cascade: false }),
		$.csscomb(),
		$.cssnano(),
		ENV.dev($.sourcemaps.write()),
		gulp.dest('./local/templates/assets/css/')
	]);

	combined.on('error', console.error.bind(console));
	return combined;
});

gulp.task('libs', () => {
	let combined = combine.obj([
		gulp.src('./source-markup/js/libs.js'),
		fileinclude('@@'),
		$.uglify(),
		gulp.dest('./local/templates/assets/js/')
	]);

	combined.on('error', console.error.bind(console));
	return combined;
});

gulp.task('scripts', () => {
	let combined = combine.obj([
		gulp.src(['./source-markup/js/*.js', '!./source-markup/js/libs.js']),
		ENV.dev($.sourcemaps.init()),
		$.babel({
			presets: ['env'],
			plugins: ['transform-object-rest-spread']
		}),
		$.uglify(),
		ENV.dev($.sourcemaps.write()),
		gulp.dest('./local/templates/assets/js/')
	]);

	combined.on('error', console.error.bind(console));
	return combined;
});

gulp.task('img', () => {
	let combined = combine.obj([
		gulp.src('./source-markup/img/**/*.*'),
		$.imagemin(),
		gulp.dest('./local/templates/assets/img/')
	]);

	combined.on('error', console.error.bind(console));
	return combined;
});

gulp.task('pictures', () => {
	let combined = combine.obj([
		gulp.src('./source-markup/pictures/**/*.*'),
		$.imagemin(),
		gulp.dest('./local/templates/assets/pictures/')
	]);

	combined.on('error', console.error.bind(console));
	return combined;
});

gulp.task('icons', function () {
	return gulp
		.src('source-markup/icons/**/*.svg')
		.pipe($.svgmin(function (file) {
			var prefix = path.basename(file.relative, path.extname(file.relative));
			
			return {
				plugins: [{
					cleanupIDs: {
					prefix: 'icon-' + prefix,
					minify: true
				}
			}]
		}
	}))
	.pipe($.cheerio({
		run: function ($, file) {
			$('style').remove();
		},
		parserOptions: { xmlMode: true }
	}))
	.pipe($.svgstore())
	.pipe(gulp.dest('./local/templates/assets/img'));
});

gulp.task('fonts', () => {
	let combined = combine.obj([
		gulp.src('./source-markup/fonts/*.*'),
		gulp.dest('./local/templates/assets/fonts/')
	]);

	combined.on('error', console.error.bind(console));
	return combined;
});

gulp.task('video', () => {
	let combined = combine.obj([
		gulp.src('./source-markup/video/**/*.*'),
		gulp.dest('./local/templates/assets/video/')
	]);

	combined.on('error', console.error.bind(console));
	return combined;
});

gulp.task('clean', (cb) => {
	rimraf('./local/templates/assets/', cb);
});

gulp.task('build', [
	'styles',
	'libs',
	'scripts',
	'img',
	'pictures',
	'icons',
	'fonts',
]);

gulp.task('watch', () => {

	$.watch(['source-markup/css/**/*.*'], function() {
		gulp.start('styles');
		browserSync.reload();
	});

	$.watch(['source-markup/js/vendor/*.*', 'source-markup/js/libs.js'], function() {
		gulp.start('libs');
		browserSync.reload();
	});

	$.watch(['source-markup/js/**/*.js', '!source-markup/js/libs.js'], function() {
		gulp.start('scripts');
		browserSync.reload();
	});

	$.watch(['source-markup/img/**/*.*'], function() {
		gulp.start('img');
		browserSync.reload();
	});

	$.watch(['source-markup/pictures/**/*.*'], function() {
		gulp.start('pictures');
		browserSync.reload();
	});

	$.watch(['source-markup/icons/**/*.*'], function() {
		gulp.start('icons');
		browserSync.reload();
	});

	$.watch(['source-markup/fonts/**/*.*'], function() {
		gulp.start('fonts');
		browserSync.reload();
	});

	$.watch(['source-markup/video/**/*.*'], function() {
		gulp.start('video');
		browserSync.reload();
	});
});

gulp.task('server', () => {
	browserSync.init({
		server: { baseDir: "./local/templates/assets/" },
		port: 9000
	});
});

gulp.task('default', ['build', 'watch']);