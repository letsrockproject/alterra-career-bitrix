<?
include_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404", "Y");

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

$APPLICATION->SetTitle("Страница не найдена");
$APPLICATION->AddChainItem($APPLICATION->GetTitle());?>
    <div class="section section_page_not-found">
        <div class="not-found container">
            <div class="not-found__inner">404</div>
            <div class="not-found__sub-text">Возможно вы ввели неверный адрес, либо страница удалена.</div>
            <div class="not-found__line"></div>
            <p class="not-found__paragraph">Если вы искали вакансии наших магазинов, вы можете посмотреть их в
                разделе
                <a class="not-found__link" href="/jobs/">Вакансии.</a>
            </p>
            <p class="not-found__paragraph">Если вас интересует информация о нас, загляните в раздел
                <a class="not-found__link" href="/about/">О компании.</a>
            </p>
            <p class="not-found__paragraph">Узнай о преимуществах работы в компании «Альтерра» в разделе
                <a class="not-found__link" href="/benefits/">Преимущества.</a>
            </p>
            <p class="not-found__paragraph">Или позвонить по телефону горячей линии. Звонок по территории РФ
                бесплатный: 8 (800) 2000-700</p>
            <a class="btn btn_color_red not-found__btn" href="/jobs/">Посмотреть все вакансии</a>
        </div>
    </div>
<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>