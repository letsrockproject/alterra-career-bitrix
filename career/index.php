<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Карьера в Альтерре"); ?>
    <div class="section">
        <div class="section__inner container">
            <div class="text-page">
                <p>Один из самых ценных активов компании – это ее сотрудники. Именно от людей зависит, насколько
                    фирма
                    будет успешной, какие позиции займет на рынке. Наша компания обеспечивает более 300 рабочих
                    мест.</p>
                <p>Мы гарантируем постоянное развитие, достойный уровень заработной платы.</p>
                <p>Одно из основных направлений нашей политики в области персонала – создание условий для
                    максимально
                    эффективной работы, для профессионального роста.</p>
                <p>Компания «Альтерра» заинтересована в развитии сотрудников. В компании много примеров карьерного
                    роста от рядового сотрудника до руководителя. У Вас также есть шанс реализовать карьерные и
                    финансовые амбиции и добиваться большего.</p>
            </div>
            <a href="/request/" class="btn btn_margined btn_color_yellow">Отправить заявку</a>
            <a href="/jobs/" class="btn btn_margined btn_color_red">Посмотреть все вакансии</a>
            <img class="section__banner" src="<?=ASSETS_HOME?>img/stairs-banner.jpg">
            <? $APPLICATION->IncludeComponent(
                "letsrock:stairs",
                "",
                Array()
            ); ?>
        </div>
    </div>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>