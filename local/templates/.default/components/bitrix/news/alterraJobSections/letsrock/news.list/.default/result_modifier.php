<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */

$sections = [];
$sectionsIds = [];
$hot = [];
$stores = [];

$arSelect = Array("ID", "NAME", "PROPERTY_UF_ADDRESS");
$arFilter = Array("IBLOCK_ID" => IntVal(7), "ACTIVE" => "Y");
$obSection = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter, false, Array("nPageSize" => 50), $arSelect);

while ($res = $obSection->GetNext()) {
    $stores[$res['ID']] = $res;
}

foreach ($arResult['ITEMS'] as $item) {
    if (array_key_exists($item['PROPERTIES']['UF_STORE']['VALUE'], $stores)) {
        $item['ADDRESS'] = $stores[$item['PROPERTIES']['UF_STORE']['VALUE']]['PROPERTY_UF_ADDRESS_VALUE'];
    }

    $sections[$item['IBLOCK_SECTION_ID']]['ITEMS'][] = $item;

    if (!in_array($item['IBLOCK_SECTION_ID'], $sectionsIds)) {
        $sectionsIds[] = $item['IBLOCK_SECTION_ID'];
    }

    if ($item['PROPERTIES']['UF_HOT']['VALUE_ENUM'] == 'true') {
        $hot[] = $item;
    }
}

$arFilter = [
    'IBLOCK_ID' => $arResult['ID'],
    'GLOBAL_ACTIVE' => 'Y',
    'CNT_ACTIVE' => 'Y',
    'ID' => $sectionsIds
];
$obSection = CIBlockSection::GetList(["SORT" => "ASC"], $arFilter, true);
$sectionsList = [];

while ($res = $obSection->GetNext()) {
    if (array_key_exists($res['ID'], $sections)) {
        $sections[$res['ID']]['SECTION_NAME'] = $res['NAME'];
    }
}


$arResult['ITEMS'] = $sections;
$arResult['HOT_ITEMS'] = $hot;