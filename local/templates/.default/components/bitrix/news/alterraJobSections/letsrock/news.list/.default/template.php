<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="section section_mobile_no-pad section_jobs">
    <div class="section__inner container">
        <div class="job-accordion js-accordion">
            <? if (count($arResult['HOT_ITEMS']) > 0): ?>
                <div class="job-accordion__item js-accordion-item">
                    <a href="javascript:void(0);"
                       class="job-accordion__title js-accordion-link job-accordion__title_type_hot">Горящие вакансии
                        <span class="job-accordion__count"> <?= count($arResult['HOT_ITEMS']) ?></span></a>
                    <ul class="job-accordion__list js-accordion-list">
                        <? foreach ($arResult['HOT_ITEMS'] as $item): ?>
                            <li class="job-accordion__list-item"><a href="<?= $item['DETAIL_PAGE_URL'] ?>"
                                                                    class="job-accordion__job-link"><?= $item['NAME'] ?>, <?= $item['PROPERTIES']['UF_SALARY']['VALUE'] ?><?= (empty($item['ADDRESS'])) ? "" : ", " . $item['ADDRESS'] ?></a>
                            </li>
                        <? endforeach; ?>
                    </ul>
                </div>
            <? endif; ?>
            <? foreach ($arResult['ITEMS'] as $section): ?>
                <div class="job-accordion__item js-accordion-item">
                    <a href="javascript:void(0);"
                       class="job-accordion__title js-accordion-link"><?= $section['SECTION_NAME'] ?>
                        <span class="job-accordion__count"> <?= count($section['ITEMS']) ?></span></a>
                    <ul class="job-accordion__list js-accordion-list">
                        <? foreach ($section['ITEMS'] as $item): ?>
                            <li class="job-accordion__list-item"><a href="<?= $item['DETAIL_PAGE_URL'] ?>"
                                                                    class="job-accordion__job-link"><?= $item['NAME'] ?>, <?= $item['PROPERTIES']['UF_SALARY']['VALUE'] ?><?= (empty($item['ADDRESS'])) ? "" : ", " . $item['ADDRESS'] ?></a>
                            </li>
                        <? endforeach; ?>
                    </ul>
                </div>
            <? endforeach; ?>
        </div>
    </div>
</div>


