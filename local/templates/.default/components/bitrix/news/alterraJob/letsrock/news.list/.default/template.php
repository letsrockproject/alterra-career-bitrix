<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="section section_mobile_no-pad section_jobs">
    <div class="section__inner container">
        <div class="cart-table cart-table_size_little">
            <? foreach ($arResult["ITEMS"] as $arItem): ?>
                <div class="cart cart_size_little">
                    <div class="cart__image">
                        <a href="<?= $arItem['DETAIL_PAGE_URL'] ?>">
                            <img src="<?= $arItem['PREVIEW_PICTURE']['SRC'] ?>">
                        </a>
                    </div>
                    <div class="cart__text-block">
                        <a class="cart__title-link" href="<?= $arItem['DETAIL_PAGE_URL'] ?>">
                            <h4 class="cart__title"><?= $arItem['NAME'] ?></h4>
                        </a>
                        <p class="cart__subtitle"><?= $arItem['PROPERTIES']['UF_SALARY']['VALUE'] ?></p>
                        <a class="btn btn_color_invert-orange cart__btn" href="<?= $arItem['DETAIL_PAGE_URL'] ?>">Подробнее</a>
                        <a class="cart__link" href="<?= $arItem['DETAIL_PAGE_URL'] ?>">Подробнее</a>
                    </div>
                </div>
            <? endforeach; ?>
        </div>
    </div>
</div>

