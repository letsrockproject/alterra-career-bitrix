<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

CModule::IncludeModule("iblock");
const IBLOCK_ID = 5;
$arFilter = Array(
    'IBLOCK_ID' => IBLOCK_ID,
    'GLOBAL_ACTIVE' => 'Y');
$obSection = CIBlockSection::GetTreeList($arFilter);
$sectionsList = [];

while ($arResult = $obSection->GetNext()) {
    if ($arResult['DEPTH_LEVEL'] == 1) {
        $sectionsList[$arResult['ID']] = $arResult['NAME'];
    }
}

$arTemplateParameters = array(
    "SECTION_ID" => Array(
        "NAME" => "Выберите раздел",
        "TYPE" => "LIST",
        "VALUES" => $sectionsList,
    )
);

?>