<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="section">
    <div class="section__inner container">
        <p class="section__subtitle">В основе корпоративной культуры «Альтерра» лежат ценности Компании:
            честность, лидерство, взаимоуважение и доверие, менталитет победителя и ответственного собственника.
            Направления и задачи развития корпоративной культуры определяются Стратегией Компании и вызовами, с
            которыми сталкивается отрасль по продаже строительных материалов.
        </p>

        <div class="cart-table cart-table_size_big">
            <? foreach ($arResult['DISPLAY_PROPERTIES']['UF_PHOTOS']['FILE_VALUE'] as $item): ?>
                <a href="<?= $item['SRC'] ?>" data-lightbox="photo" class="cart cart_size_big cart_type_static cart_image-only">
                    <div class="cart__image-big">
                        <img src="<?= $item['SRC'] ?>">
                    </div>
                </a>
            <? endforeach; ?>

            <?
            $APPLICATION->IncludeComponent("bitrix:main.pagenavigation", "", array("NAV_OBJECT" => $arResult['nav'], "SEF_MODE" => "N",), false);
            ?>
        </div>
    </div>
</div>