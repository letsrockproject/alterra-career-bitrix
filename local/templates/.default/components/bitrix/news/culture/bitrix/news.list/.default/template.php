<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="section">
    <div class="section__inner container">
        <p class="section__subtitle">В основе корпоративной культуры «Альтерра» лежат ценности Компании:
            честность, лидерство, взаимоуважение и доверие, менталитет победителя и ответственного собственника.
            Направления и задачи развития корпоративной культуры определяются Стратегией Компании и вызовами, с
            которыми сталкивается отрасль по продаже строительных материалов.
        </p>

        <div class="cart-table cart-table_size_big">
            <? foreach ($arResult["ITEMS"] as $arItem): ?>
                <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="cart cart_size_big cart_type_static">
                    <div class="cart__image">
                        <img src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>">
                    </div>
                    <div class="cart__text-block cart__text-block_size_static">
                        <h4 class="cart__title"><?= $arItem["NAME"] ?></h4>
                        <p class="cart__date"><?= $arItem["ACTIVE_FROM"] ?></p>
                    </div>
                </a>
            <? endforeach; ?>
        </div>
    </div>
</div>




