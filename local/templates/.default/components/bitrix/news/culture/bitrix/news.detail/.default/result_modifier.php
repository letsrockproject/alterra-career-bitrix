<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var CBitrixComponentTemplate $this */
$photos = $arResult['DISPLAY_PROPERTIES']['UF_PHOTOS']['FILE_VALUE'];

const PAGE_SIZE = 6;

$nav = new \Bitrix\Main\UI\PageNavigation("nav");
$nav->setPageSize(PAGE_SIZE)->initFromUri();

$nav->setRecordCount(count($photos));

$arResult['nav'] = $nav;
$offset = $nav->getOffset();

if (count($arResult['DISPLAY_PROPERTIES']['UF_PHOTOS']['FILE_VALUE']) > PAGE_SIZE) {
    $arResult['DISPLAY_PROPERTIES']['UF_PHOTOS']['FILE_VALUE'] = array_slice($arResult['DISPLAY_PROPERTIES']['UF_PHOTOS']['FILE_VALUE'], $offset, PAGE_SIZE);
}

return $arResult;

?>
