<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$APPLICATION->SetPageProperty("job", $arResult['PROPERTIES']['UF_JOB']['VALUE']);
?>

<div class="section">
    <div class="section__inner container">
        <div class="text-page">
            <?= $arResult['DETAIL_TEXT'] ?>
        </div>
    </div>
</div>
