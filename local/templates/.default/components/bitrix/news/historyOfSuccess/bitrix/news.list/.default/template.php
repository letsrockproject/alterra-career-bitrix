<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<h3 class="section__title section__title_align_left section__title_page_success">Как сделать карьеру в
    «Альтерре»</h3>
<p class="section__subtitle">
    Ты знал, что, согласно статистике, около 90% всех руководителей разных компаний начинали свою карьеру
    простыми продавцами? «Альтерра» предоставляет все возможности для карьерного роста тем, кто хочет
    развиваться, учиться и однажды стать успешным руководителем. Все зависит от тебя ! «Альтерра» ценит
    своих
    сотрудников и старается предоставить им все возможности для карьерного роста. Поэтому, когда в компании
    открывается вакансия руководителя, то в первую очередь рассматриваются уже работающие сотрудники с
    необходимыми навыками, которые могут стать успешными руководителями и применять весь свой имеющийся
    опыт.
</p>
<div class="success-slider js-slider-success" data-slick='{"slidesToShow": 3, "slidesToScroll": 1, "variableWidth": true}'>
    <div class="success-slider__controls success-slider__controls_prev js-slider-prev"></div>
    <? foreach ($arResult["ITEMS"] as $arItem): ?>
        <div class="success-slider__slide js-slide">
            <div class="success-slider__image">
                <img src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>">
            </div>
            <h4 class="success-slider__name"><?= $arItem["NAME"] ?></h4>
            <p class="success-slider__subtitle"><?= $arItem['PROPERTIES']['UF_JOB']['VALUE'] ?></p>
            <span class="success-slider__quote"></span>
            <p class="success-slider__history">
                <?= $arItem["PREVIEW_TEXT"] ?>
            </p>
            <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="success-slider__link">Читать далее</a>
            <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="success-slider__link-wrap"></a>
        </div>
    <? endforeach; ?>
    <div class="success-slider__controls success-slider__controls_next js-slider-next"></div>
</div>





