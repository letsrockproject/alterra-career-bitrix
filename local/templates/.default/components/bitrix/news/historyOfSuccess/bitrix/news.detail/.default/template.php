<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$APPLICATION->SetPageProperty("job", $arResult['PROPERTIES']['UF_JOB']['VALUE']);
?>
<div class="section section_not-top-pad">
    <div class="section__inner section__inner_many-column container">
        <div class="section__content">
            <div class="text-page">
                <?= $arResult['DETAIL_TEXT'] ?>
                <br><a href="/request/" class="btn btn_color_yellow">Отправить заявку</a>
            </div>
        </div>
        <? if (!empty($arResult['DETAIL_PICTURE'])): ?>
            <div class="section__side-bar side-bar side-bar_right success">
                <div class="success__image">
                    <img src="<?= $arResult['DETAIL_PICTURE']['SRC'] ?>">
                </div>
            </div>
        <? endif; ?>
    </div>
</div>
