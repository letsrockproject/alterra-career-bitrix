<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$APPLICATION->SetTitle($arResult['NAME']);
$APPLICATION->AddChainItem($arResult['SECTION_INFO']['NAME'], 'javascript:void(0)');
?>
<div class="section section_not-top-pad">
    <div class="section__inner section__inner_many-column container">
        <div class="section__content">
            <div class="job-details-block">
                <div class="job-details-block__inner">
                    <div class="job-details-block__information">
                        <p class="job-details-block__information-name">город</p>
                        <p class="job-details-block__information-text"><?= $arResult['CITY'] ?></p>
                    </div>
                    <div class="job-details-block__information job-details-block__information_type_money">
                        <p class="job-details-block__information-name">зарплата</p>
                        <p class="job-details-block__information-text"><?= $arResult['SALARY'] ?></p>
                    </div>
                </div>
                <a href="/request/?job=<?= $arResult['ID'] ?>" class="btn btn_color_yellow">Отправить заявку</a>
            </div>
            <div class="text-page text-page_padding_top">
                <?= $arResult['DETAIL_TEXT'] ?>
                <a href="/request/?job=<?= $arResult['ID'] ?>" class="btn btn_color_yellow">Отправить заявку</a>
            </div>
        </div>
        <div class="section__side-bar side-bar">
            <img src="<?= $arResult['DETAIL_PICTURE']['SRC'] ?>" class="side-bar__img">
            <? foreach ($arResult['MORE_PHOTO'] as $photo): ?>
                <img src="<?= "/" . COption::GetOptionString("main", "upload_dir", "upload") . "/" . $photo["SUBDIR"] . "/" . $photo["FILE_NAME"] ?>" class="side-bar__img">
            <? endforeach; ?>
            <div class="help-block">
                <p class="help-block__title">Возникли вопросы? Звоните нам!</p>
                <a class="help-block__tel-link" href="tel:+73853261500">+7 (38532) 615 - 00</a>
                <a class="help-block__tel-link" href="tel:+79635080641">+7-963-508-06-41</a>
            </div>
        </div>
    </div>
</div>
