<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */

/** @var CBitrixComponentTemplate $this */


use Bitrix\Main\Loader;
use Letsrock\Lib\Models\City;

Loader::includeModule('letsrock.lib');

CModule::IncludeModule('highloadblock');
$city = new City();


$res = CIBlockSection::GetList(["SORT" => "ASC"], ['ID' => $arParams['SECTION_ID'], 'IBLOCK_ID' => $arParams['IBLOCK_ID']]);
$arResult['SECTION_INFO'] = $res->fetch();

$arSelect = ["ID", "NAME", "PROPERTY_UF_ADDRESS", "PROPERTY_UF_CITY"];
$arFilter = ["IBLOCK_ID" => IntVal(7), "ACTIVE" => "Y", "ID" => $arResult['PROPERTIES']['UF_STORE']['VALUE']];
$obSection = CIBlockElement::GetList([], $arFilter, false, ["nPageSize" => 50], $arSelect);
$store = null;

while ($res = $obSection->GetNext()) {
    if (!in_array($res['PROPERTY_UF_CITY_VALUE'], $store)) {
        $store[] = $res['PROPERTY_UF_CITY_VALUE'];
    }
}

$stores = $city->getListCitys($store);

foreach ($stores as $key=>$town) {
    $stores[$key] = $town['UF_NAME'];
}

$arResult['CITY'] = implode(', ', $stores);


$arResult['SALARY'] = $arResult['PROPERTIES']['UF_SALARY']['VALUE'];

$res = CFile::GetList(["id" => "asc"], ["@ID" => implode(',', $arResult['PROPERTIES']['UF_IMAGE']['VALUE'])]);

while ($res_arr = $res->GetNext()) {
    $arResult['MORE_PHOTO'][] = $res_arr;
}

return $arResult;