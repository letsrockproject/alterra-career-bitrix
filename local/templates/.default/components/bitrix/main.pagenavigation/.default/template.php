<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @var CBitrixComponentTemplate $this */

/** @var PageNavigationComponent $component */
$component = $this->getComponent();

$this->setFrameMode(true);

?>
<div class="pagination">
    <div class="pagination__inner">
        <? for ($i = 1; $i <= $arResult['PAGE_COUNT']; $i++): ?>
            <? if ($i == $arResult['CURRENT_PAGE']): ?>
                <a href="<?=$component->replaceUrlTemplate($i)?>" class="pagination__link pagination__link_active"><?=$i?></a>
            <? else: ?>
                <a href="<?=$component->replaceUrlTemplate($i)?>" class="pagination__link"><?=$i?></a>
            <? endif; ?>
        <? endfor; ?>
    </div>
</div>


