<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
?>
<ul class="header-menu header-menu_type_mobile js-mobile-menu-list">
    <li class="header-menu__item header-menu__item_type_mobile"><a href="javascript:void(0)" class="header-menu__link header-menu__link_type_mobile js-mobile-city-link">Мой город:
            <? $APPLICATION->IncludeComponent(
                "letsrock:cityChooser",
                "mobileMini",
                Array()
            ); ?>
            </a></li>
    <? foreach ($arResult as $item): ?>
    <li class="header-menu__item header-menu__item_type_mobile"><a href="<?= $item['LINK'] ?>" class="header-menu__link header-menu__link_type_mobile"><?= $item['TEXT'] ?></a></li>
<? endforeach; ?>
</ul>