<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
?>
<div class="jobs-tabs">
    <ul class="jobs-tabs__inner container">
        <? foreach ($arResult as $item): ?>
            <li class="jobs-tabs__item"><a href="<?= $item['LINK'] ?>" class="jobs-tabs__link<?= $item['SELECTED'] ? " active" : "" ?>"><?= $item['TEXT'] ?></a></li>
        <? endforeach; ?>
    </ul>
</div>