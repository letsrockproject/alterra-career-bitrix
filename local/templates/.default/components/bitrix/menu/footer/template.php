<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
?>
<div class="footer-menu js-footer-menu">
    <a href="javascript:void(0);"
       class="footer-menu__title footer-menu__title_type_link js-footer-menu-link"><?=$arParams['TITLE']?></a>
    <h6 class="footer-menu__title footer-menu__title_type_text"><?=$arParams['TITLE']?></h6>
    <ul class="footer-menu__inner js-footer-menu-body">
        <? foreach ($arResult as $item): ?>
            <li class="footer-menu__item"><a href="<?= $item['LINK'] ?>" class="footer-menu__link"><?= $item['TEXT'] ?></a>
            </li>
        <? endforeach; ?>
    </ul>
</div>