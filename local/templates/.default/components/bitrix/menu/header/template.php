<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
?>
<ul class="header-menu header-menu_type_desktop header__header-menu">
<? foreach ($arResult as $item): ?>
    <li class="header-menu__item header-menu__item_type_desktop"><a href="<?= $item['LINK'] ?>" class="header-menu__link header-menu__link_type_desktop"><?= $item['TEXT'] ?></a></li>
<? endforeach; ?>
</ul>
