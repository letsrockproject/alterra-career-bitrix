<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? if (!empty($arResult)): ?>
    <div class="section">
        <div class="section__inner container">
            <div class="cart-table cart-table_size_big">
                <? foreach ($arResult as $arItem): ?>
                    <div class="cart cart_size_big">
                        <div class="cart__image">
                            <img src="<?= ASSETS_HOME .  'img/' . $arItem['PARAMS']['IMAGE']?>">
                        </div>
                        <div class="cart__text-block cart__text-block_size_half">
                            <h4 class="cart__title"><?= $arItem["TEXT"] ?></h4>
                            <a class="btn btn_color_invert-orange cart__btn" href="<?= $arItem["LINK"] ?>">Подробнее</a>
                        </div>
                    </div>
                <? endforeach ?>
            </div>
        </div>
    </div>
<? endif ?>