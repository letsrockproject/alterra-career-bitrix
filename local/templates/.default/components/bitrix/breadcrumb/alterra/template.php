<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/**
 * @global CMain $APPLICATION
 */

global $APPLICATION;

//delayed function must return a string
if(empty($arResult))
	return "";

$strReturn = '<div class="breadcrumbs"><ul class="breadcrumbs__inner">';

foreach ($arResult as $key => $crumb) {
    if ($key + 1 < count($arResult)) {
        $strReturn .= '<li class="breadcrumbs__item"><a href="' . $crumb['LINK'] .'" class="breadcrumbs__link">' . $crumb['TITLE'] .'</a></li>';
    } else {
        $strReturn .= '<li class="breadcrumbs__item">' . $crumb['TITLE'] .'</li>';
    }
}

$strReturn .= '</ul></div>';

return $strReturn;

?>
