<?php

namespace Letsrock\Lib\Models;

use Bitrix\Main\Loader;

Loader::includeModule('iblock');

use Bitrix\Main\Context;

class City {
    private $cookieCityId;

    function __construct() {
        $request = Context::getCurrent()->getRequest();
        $this->cookieCityId = $request->getCookie('CITY');

        if (empty($this->cookieCityId)) {
            $this->cookieCityId = 1;
        }
    }

    /**
     * @return array|bool|false
     */
    function getCurrentCity() {
        $hlblock = new HLBlock(HL_CITY);
        return $hlblock->getSingleItemById($this->cookieCityId);
    }

    /**
     * @param $id
     * @return array|bool|false
     */
    function getCity($id) {
        $hlblock = new HLBlock(HL_CITY);
        return $hlblock->getSingleItemById($id);
    }

    /**
     * @return array|bool
     */
    function getListCitys($xmlIds = false) {
        $hlblock = new HLBlock(HL_CITY);

        if ($xmlIds) {
            return $hlblock->get([
                'UF_XML_ID' => $xmlIds
            ]);
        } else {
            return $hlblock->get();
        }
    }

    /**
     * @param $id
     * @param bool $onlyId
     * @return array
     */
    function getStoreById($id, $onlyId = false) {
        Loader::IncludeModule("iblock");
        $arSelect = ["ID", "NAME", "PROPERTY_UF_CITY"];
        $arFilter = ["IBLOCK_ID" => IB_STORE, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y", 'ID' => $id];
        $res = \CIBlockElement::GetList(["SORT" => "ASC"], $arFilter, false, ["nPageSize" => 50], $arSelect);

        return $res->GetNextElement();
    }

    /**
     * @param $id
     * @param bool $onlyId
     * @return array
     */
    function getStoresByXmlId($id, $onlyId = false) {
        Loader::IncludeModule("iblock");
        $arSelect = ["ID", "NAME", "PROPERTY_UF_CITY"];
        $arFilter = ["IBLOCK_ID" => IB_STORE, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y", 'PROPERTY_UF_CITY' => $id];
        $res = \CIBlockElement::GetList(["SORT" => "ASC"], $arFilter, false, ["nPageSize" => 50], $arSelect);

        $stores = [];

        while ($ob = $res->GetNextElement()) {
            $tmp = $ob->GetFields();

            if ($onlyId) {
                $stores[] = $tmp['ID'];
            } else {
                $stores[] = $tmp;
            }
        }

        return $stores;
    }

    /**
     * @return array
     */
    function getStoresIDByCurrent() {
        $currentCityId = ($this->getCurrentCity())['UF_XML_ID'];
        $stores = $this->getStoresByXmlId($currentCityId, true);
        $storesId = [];

        foreach ($stores as $store) {
            $storesId[] = $store['ID'];
        }

        return $stores;
    }

    /**
     * Получить вакансии по городу
     *
     * если id не задан - по текущему городу
     * @param $id
     * @return mixed
     */
    function getJobsByCity($id = false) {
        if ($id) {
            $cityXmlId = ($this->getCity($id))['UF_XML_ID']; //получаем xml_id по id города
        } else {
            $cityXmlId = ($this->getCurrentCity())['UF_XML_ID']; //получаем xml_id текущего города
        }

        $storesId = $this->getStoresByXmlId($cityXmlId, true); //получаем все магазины текущего города

        Loader::IncludeModule("iblock");
        $arSelect = [];
        $arFilter = ["IBLOCK_ID" => IB_JOB, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y", 'PROPERTY_UF_STORE' => $storesId];
        $res = \CIBlockElement::GetList(["SORT" => "ASC"], $arFilter, false, ["nPageSize" => 50], $arSelect);
        $jobs = [];

        while ($ob = $res->GetNextElement()) {
            $jobs[] = $ob->GetFields();
        }

        return $jobs;
    }

    /**
     * Получить список мест для собеседования по городу
     *
     * если id не задан - по текущему городу
     * @param $id
     * @return mixed
     */
    function getReviewPlacesByCity($id = false) {
        if ($id) {
            $cityXmlId = ($this->getCity($id))['UF_XML_ID']; //получаем xml_id по id города
        } else {
            $cityXmlId = ($this->getCurrentCity())['UF_XML_ID']; //получаем xml_id текущего города
        }

        Loader::IncludeModule("iblock");
        $arSelect = ['ID', 'NAME', 'PROPERTY_UF_ADDRESS', "PROPERTY_UF_MAP"];
        $arFilter = ["IBLOCK_ID" => IB_STORE, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y", 'PROPERTY_UF_INTERVIEW' => $cityXmlId];
        $res = \CIBlockElement::GetList(["SORT" => "ASC"], $arFilter, false, ["nPageSize" => 50], $arSelect);
        $jobs = [];

        while ($ob = $res->GetNextElement()) {
            $jobs[] = $ob->GetFields();
        }

        return $jobs;
    }
}