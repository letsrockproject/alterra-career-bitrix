<?php
namespace Letsrock\Lib\Models;

use Bitrix\Main\Loader;

Loader::includeModule('iblock');

class Form {
    /**
     * Возвращает список вакансий и мест собеседования для текущего города
     * или любого города по id
     * @param bool $id
     * @return array
     */
    function getDataByCityId($id = false) {
        $arResult = [];
        $city = new City();
        $arResult['CURRENT_CITY'] = $city->getCurrentCity();
        $arResult['LIST_JOBS'] = $city->getJobsByCity($id);
        $arResult['LIST_REVIEW_PLACES'] = $city->getReviewPlacesByCity($id);
        $listCity = $city->getListCitys();

        foreach ($listCity as $key => $city) {
            if ($city['ID'] == $arResult['CURRENT_CITY']['ID']) {
                unset($listCity[$key]);
                array_unshift($listCity, $city);
            }
        }

        $arResult['LIST_CITY'] = $listCity;

        return $arResult;
    }
}