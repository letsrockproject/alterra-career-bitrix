<?
require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main\Loader;
use \Letsrock\Lib\Models\Form;
Loader::includeModule('letsrock.lib');

if (isset($_POST['CITY_ID'])) {
    $formData = new Form();
    $formInfo = $formData->getDataByCityId($_POST['CITY_ID']);
    echo json_encode($formInfo);
}