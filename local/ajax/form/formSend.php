<?
require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main\Loader;
use Letsrock\Lib\Models\HLBlock;

Loader::includeModule('letsrock.lib');
Loader::includeModule('iblock');


if (!empty($_POST['name'])) {
    $response = $_POST;
    $arFile = [];

    move_uploaded_file($_FILES['file']['tmp_name'], $_SERVER["DOCUMENT_ROOT"] . '/upload/tmp/' . $_FILES['file']['name']);
    if (file_exists($_SERVER["DOCUMENT_ROOT"] . '/upload/tmp/' . $_FILES['file']['name'])) {
        $arFile = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"] . '/upload/tmp/' . $_FILES['file']['name']);
    }

    $fileId = CFile::SaveFile($arFile, '/form/');
    $property_enums = CIBlockPropertyEnum::GetList(["DEF" => "DESC", "SORT" => "ASC"], ["IBLOCK_ID" => IB_RESUME, "CODE" => "UF_SEX"]);

    while ($enum_fields = $property_enums->GetNext()) {
        if ($response['sex'] == $enum_fields["XML_ID"]) {
            $response['sex'] = $enum_fields["ID"];
        }
    }

    $hlBlock = new HLBlock(HL_CITY);
    $response['town'] = $hlBlock->getSingleItemById($response['town']);

    $el = new CIBlockElement;

    $props = [
        'UF_NAME'         => $response['name'],
        'UF_YEAR_B'       => $response['year'],
        'UF_DATE_TIME'    => $response['date'] . " " . $response['time'],
        'UF_REVIEW_PLACE' => $response['address'],
        'UF_JOB'          => $response['job'],
        'UF_SEX'          => $response['sex'],
        'UF_TEL'          => $response['tel'],
        'UF_EMAIL'        => $response['email'],
        'UF_CITY'         => $response['town']['UF_XML_ID'],
        'UF_DOC'          => $fileId,
    ];

    $arLoadProductArray = [
        "IBLOCK_SECTION_ID" => false,
        "IBLOCK_ID"         => IB_RESUME,
        "PROPERTY_VALUES"   => $props,
        "NAME"              => $response['town']['UF_NAME'] . ', ' . $response['name'],
        "ACTIVE"            => "Y",
    ];

    $cityModel = new \Letsrock\Lib\Models\City();
    $store = $cityModel->getStoreById($response['address']);
    $store = $store->getFields();
    $file = "";

    $arEventFields = [
        "NAME"  => htmlspecialcharsEx($response['name']),
        "YEAR"  => htmlspecialcharsEx($response['year']),
        "TEL"   => htmlspecialcharsEx($response['tel']),
        "EMAIL" => htmlspecialcharsEx($response['email']),
        "DATE"  => htmlspecialcharsEx($response['date'] . " " . $response['time']),
        "CITY"  => $response['town']['UF_NAME'],
        "PLACE" => $store['NAME'],
    ];

    if (CModule::IncludeModule("main")) {
        if (CEvent::Send("NEW_REQUEST", "s1", $arEventFields, "Y", "", [$fileId])) {
            echo "ok";
        }
    }

    if ($id = $el->Add($arLoadProductArray)) {
        echo json_encode("Ok");
    } else {
        echo json_encode("Error: " . $el->LAST_ERROR);
    }
}