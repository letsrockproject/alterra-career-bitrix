<footer class="footer js-footer">
    <div class="footer__inner container container_no-pad">
        <a href="https://tg-alterra.ru/" class="footer__logo">
            <img src="<?=ASSETS_HOME?>img/logo-footer.svg" alt="Альтерра">
        </a>
        <? $APPLICATION->IncludeComponent("bitrix:menu", "footer", Array(
            "ROOT_MENU_TYPE" => "jobs",
            "MAX_LEVEL" => "1",
            "USE_EXT" => "Y",
            "TITLE" => "Вакансии"
        ),
            false
        ); ?>

        <? $APPLICATION->IncludeComponent("bitrix:menu", "footer", Array(
            "ROOT_MENU_TYPE" => "about",
            "MAX_LEVEL" => "1",
            "USE_EXT" => "Y",
            "TITLE" => "О компании"
        ),
            false
        ); ?>

        <? $APPLICATION->IncludeComponent("bitrix:menu", "footer", Array(
            "ROOT_MENU_TYPE" => "advantages",
            "MAX_LEVEL" => "1",
            "USE_EXT" => "Y",
            "TITLE" => "Преимущества"
        ),
            false
        ); ?>
        <div class="contacts-block footer__contacts">
            <a href="/request/" class="btn btn_color_yellow btn_font_light contacts-block_btn">Заполнить анкету</a>
            <span class="contacts-block__telephone-title">или позвони по телефону</span>
            <a href="tel:+7 (38532) 6-15-00" class="contacts-block__telephone">+7 (38532) 6-15-00</a>
            <a href="tel:+7 913 210 39-15" class="contacts-block__telephone">+7 913 210 39-15</a>
            <a href="tel:+7 963 508 06-41" class="contacts-block__telephone">+7 963 508 06-41</a>
            <div class="social contacts-block__social">
                <a href="https://vk.com/alterra22" class="social__icon social__icon_vk"></a>
                <a href="https://ok.ru/profile/574222813210" class="social__icon social__icon_ok"></a>
                <a href="https://www.instagram.com/tg_alterra/" class="social__icon social__icon_instagram"></a>
            </div>
        </div>
    </div>
    <div class="copyright-block">
        <div class="copyright-block__inner container">
            <span class="copyright-block__copyright">© <?=date('Y')?> Все права защищены</span>
            <a href="javascript:void(0);" class="copyright-block__mobile-link js-mobile-version">Мобильная версия</a>
            <a href="javascript:void(0);" class="copyright-block__mobile-link unactive js-desktop-version">Полная
                версия</a>
            <span class="copyright-block__letsrock">
                Разработка сайта -
                <a href="https://www.letsrock.pro" class="copyright-block__letsrock-logo">
                    <img src="<?=ASSETS_HOME?>img/letsrock.svg" alt="LetsRock">
                </a>
            </span>
        </div>
    </div>
</footer>

</div> <?//закрытие div.wrapper?>
<?// $APPLICATION->IncludeComponent(
//    "letsrock:cityChooser",
//    "modal",
//    Array()
//); ?>
<div id="fade"></div>
<script src="<?=ASSETS_HOME?>js/libs.js"></script>
<script src="<?=ASSETS_HOME?>js/script.js"></script>
</body>
</html>