<div class="section section_treasure">
    <div class="container">
        <div class="section__cart">
            <h3 class="section__cart-title">Узнай о ценностях компании «Альтерра» </h3>
            <a href="/treasure/" class="btn btn_color_blue section__cart-btn btn_font_light">Подробнее</a>
        </div>
    </div>
</div>