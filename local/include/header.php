<!DOCTYPE html>
<html>
<head>
    <?$APPLICATION->ShowHead();?>
    <title><?$APPLICATION->ShowTitle();?></title>
    <?
    CJSCore::Init(array("ajax"));
    ?>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
    <link rel="stylesheet" href="<?=ASSETS_HOME?>css/style.css">
    <link rel="icon" type="image/x-icon" href="/favicon.ico">
    <script src="https://api-maps.yandex.ru/2.1/?apikey=e6785424-4e42-4e69-9bb3-9d9b1a528ae1&lang=ru_RU"
            type="text/javascript"></script>
</head>
<body>
<div id="panel">
    <?$APPLICATION->ShowPanel();?>
</div>
<div class="wrapper">
    <header class="header js-header">
        <div class="header__inner container">
            <a href="javascript:void(0);" class="header__hamburger js-menu-toggle"></a>
            <a href="/" class="header__logo">
                <img src="<?=ASSETS_HOME?>img/logo.svg" alt="Альтерра">
                <span class="header__logo-text">Карьера</span>
            </a>
            <? $APPLICATION->IncludeComponent(
                "letsrock:cityChooser",
                "",
                Array()
            ); ?>
            <?$APPLICATION->IncludeComponent("bitrix:menu", "header", Array(
                "ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
                "MAX_LEVEL" => "3",	// Уровень вложенности меню
                "CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
                "USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
            ),
                false
            );?>
            <a href="/request/" class="header__btn btn btn_font_light btn_color_yellow">Заполнить анкету</a>
        </div>

        <div class="mobile-menu js-mobile-menu">
            <a href="javascript:void(0)" class="mobile-menu__exit js-menu-toggle"></a>
            <?$APPLICATION->IncludeComponent("bitrix:menu", "headerMobile", Array(
                "ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
                "MAX_LEVEL" => "3",	// Уровень вложенности меню
                "CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
                "USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
            ),
                false
            );?>
            <? $APPLICATION->IncludeComponent(
                "letsrock:cityChooser",
                "mobile",
                Array()
            ); ?>

            <div class="contacts-block contacts-block_header">
                <a href="tel:+7 (38532) 6-15-00" class="contacts-block__telephone">+7 (38532) 6-15-00</a>
                <a href="tel:7 913 210 39-15" class="contacts-block__telephone">+7 913 210 39-15</a>
                <a href="tel:+7 963 508 06-41" class="contacts-block__telephone">+7 963 508 06-41</a>
            </div>
            <a href="/request/" class="contacts-block__btn btn btn_color_yellow">Заполнить анкету</a>
        </div>

        <div class="mobile-menu mobile-menu_start js-mobile-start-city">
            <? $APPLICATION->IncludeComponent(
                "letsrock:cityChooser",
                "mobileStart",
                Array()
            ); ?>
        </div>
    </header>
    <? if ($APPLICATION->GetCurPage(false) !== '/'): ?>
        <div class="content-head container">
            <?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "alterra", Array(
                "PATH" => "",
                "SITE_ID" => "s1",
                "START_FROM" => "0",
            ),
                false
            );?>
            <div class="content-head__inner">
                <div class="title-block">
                    <h1 class="page-title"><?= $APPLICATION->ShowTitle(false) ?></h1>
                    <h4><?=$APPLICATION->ShowProperty("job");?></h4>
                </div>
                <? if ($APPLICATION->GetCurPage(false) === '/request/'): ?>
                    <div class="resume-block js-resume-block-add">
                        <p class="resume-block__text">Для более полной информации добавьте  Ваше резюме </p>
                        <a href="javascript:void(0);" class="btn btn_margined btn_color_red resume-block__btn js-file-add">Прикрепить резюме</a>
                    </div>
                    <div class="resume-block resume-block_reset js-resume-block-reset">
                        <p class="resume-block__text resume-block__text_reset js-resume-block-file-name"></p>
                        <a href="javascript:void(0);" class="btn btn_margined btn_color_gray resume-block__btn js-file-reset">Открепить резюме</a>
                    </div>
                <? endif; ?>
            </div>
        </div>
    <? endif; ?>
