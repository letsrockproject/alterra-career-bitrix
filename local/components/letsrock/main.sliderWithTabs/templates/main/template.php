<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<div class="section section_light">
    <div class="section__inner container">
        <h2 class="section__title">«Альтерра». Сделай правильный выбор!</h2>
        <div class="section__tabs tabs js-tabs">
            <div class="tabs__links-pad">
                <? foreach ($arResult as $row): ?>
                <a href="javascript:void(0)" class="tabs__link js-tabs-link">
                    <?= $row['NAME'] ?>
                </a>
                <? endforeach;?>
            </div>
            <div class="tabs__inner tabs__inner_desktop">
                <? foreach ($arResult as $row): ?>
                <div class="tabs__content js-tabs-content"
                     style='background-image: url("<?= $row['PREVIEW_PICTURE'] ?>")'>
                    <div class="tabs__text-wrap">
                        <h4 class="tabs__title"><?= $row['PROPERTY_UF_TITLE_VALUE'] ?></h4>
                        <p class="tabs__text"><?= $row['PREVIEW_TEXT'] ?></p>
                        <a href="<?= $row['PROPERTY_UF_LINK_VALUE'] ?>" class="btn btn_color_blue">Узнать подробнее</a>
                    </div>
                </div>
                <? endforeach;?>
            </div>
        </div>
    </div>
</div>

