<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>
<h3 class="header-menu__title">Выберите свой город</h3>
<ul class="header-menu_type_mobile-start header-menu_assignment_city js-mobile-city-menu">
    <? foreach ($arResult['LIST'] as $item): ?>
    <li class="header-menu__item header-menu__item_type_mobile-start"><a href="javascript:void(0);" class="header-menu__link header-menu__link_type_mobile-start js-city-choose-link" data-id="<?=$item['ID']?>"><?=$item['UF_NAME']?></a></li>
    <? endforeach; ?>
</ul>
