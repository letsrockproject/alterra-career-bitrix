<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>
<div class="header__city-choose city-choose js-location">
    <a href="javascript:void(0);" class="city-choose__city js-location-link"><i
                class="fas fa-map-marker-alt city-choose__location-icon"> </i><?= $arResult['CURRENT']['UF_NAME'] ?></a>
    <div class="city-choose__dropdown js-location-list">
        <div class="city-choose__dropdown-inner">
            <h3 class="city-choose__title">Выберите свой город</h3>
            <? foreach ($arResult['LIST'] as $item): ?>
                <? if ($item['ID'] == $arResult['CURRENT']['ID']): ?>
                    <a href="javascript:void(0);" class="city-choose__link city-choose__link_active js-city-choose-link" data-id="<?=$item['ID']?>"><?=$item['UF_NAME']?></a>
                <? else: ?>
                    <a href="javascript:void(0);" class="city-choose__link js-city-choose-link" data-id="<?=$item['ID']?>"><?=$item['UF_NAME']?></a>
                <? endif; ?>
            <? endforeach; ?>
        </div>
    </div>
</div>
