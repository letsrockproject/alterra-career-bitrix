<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>
<div class="modal" id="city-choose">
    <div class="modal__inner">
        <h3 class="modal__title">Выберите свой город</h3>
        <div class="modal__city-list js-location-select">
            <? foreach ($arResult['LIST'] as $item): ?>
                <a href="javascript:void(0);" class="modal__city-link js-city-choose-link"
                   data-id="<?= $item['ID'] ?>"><?= $item['UF_NAME'] ?></a>
            <? endforeach; ?>
        </div>
    </div>
</div>
