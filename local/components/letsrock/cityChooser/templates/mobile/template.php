<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>
<ul class="header-menu_type_mobile header-menu_assignment_city js-mobile-city-menu">
    <? foreach ($arResult['LIST'] as $item): ?>
    <li class="header-menu__item header-menu__item_type_mobile"><a href="javascript:void(0);" class="header-menu__link header-menu__link_type_mobile js-city-choose-link" data-id="<?=$item['ID']?>"><?=$item['UF_NAME']?></a></li>
    <? endforeach; ?>
</ul>
