<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Loader;
use Letsrock\Lib\Models\City;
Loader::includeModule('letsrock.lib');


class CityChooser extends CBitrixComponent
{
    public function onPrepareComponentParams($arParams)
    {
        $result = array(
            "CACHE_TYPE" => $arParams["CACHE_TYPE"],
            "CACHE_TIME" => isset($arParams["CACHE_TIME"]) ? $arParams["CACHE_TIME"] : 36000000
        );
        return $result;
    }

    public function executeComponent()
    {
        if ($this->startResultCache()) {
            $city = new City();
            $this->arResult['LIST'] = $city->getListCitys();
            $this->arResult['CURRENT'] = $city->getCurrentCity();
            $this->includeComponentTemplate();
        }

        return $this->arResult;
    }
} ?>