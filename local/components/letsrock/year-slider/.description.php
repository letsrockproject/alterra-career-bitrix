<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$arComponentDescription = array(
    "NAME" => 'Блок преимуществ',
    "DESCRIPTION" => 'Блок преимуществ',
    "PATH" => array(
        "ID" => "dv_components",
        "CHILD" => array(
            "ID" => "slider",
            "NAME" => "Блок преимуществ"
        )
    ),
);
?>