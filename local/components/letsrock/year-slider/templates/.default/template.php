<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<div class="year-slider js-slider-year">
    <div class="year-slider__controls year-slider__controls_prev js-slider-prev"></div>
    <? foreach ($arResult as $row): ?>
        <a href="javascript:void(0)" class="year-slider__slide js-slide">
            <div class="year-slider__image">
                <img src="<?= $row['PREVIEW_PICTURE'] ?>">
            </div>
            <p class="year-slider__year"><?= $row['PROPERTY_UF_YEAR_VALUE'] ?></p>
            <p class="year-slider__slide-text"><?= $row['NAME'] ?></p>
        </a>
    <? endforeach; ?>
    <div class="year-slider__controls year-slider__controls_next js-slider-next"></div>
</div>
