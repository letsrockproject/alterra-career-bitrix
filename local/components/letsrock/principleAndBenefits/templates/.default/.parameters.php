<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

CModule::IncludeModule("iblock");
const IBLOCK_ID = 5;
$arFilter = Array(
    'IBLOCK_ID' => IBLOCK_ID,
    'GLOBAL_ACTIVE' => 'Y');
$obSection = CIBlockSection::GetTreeList($arFilter);
$list = [];

$res = CIBlock::GetList(
    Array(),
    Array(
        'TYPE'=>'content',
        'SITE_ID'=>'s1',
        'ACTIVE'=>'Y',
        "CNT_ACTIVE"=>"Y",
    ), true
);

while ($arResult = $res->Fetch()) {
    $list[$arResult['ID']] = $arResult['NAME'];
}

$arTemplateParameters = array(
    "IBLOCK_ID" => Array(
        "NAME" => "Выберите раздел",
        "TYPE" => "LIST",
        "VALUES" => $list,
    )
);

?>