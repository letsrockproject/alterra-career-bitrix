<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? foreach ($arResult as $key => $row): ?>
    <? if (($key % 2) != 1): ?>
        <div class="section section_little-pad">
            <div class="section__inner section__inner_many-column container"<?= $row['PROPERTY_UF_ANCHOR_VALUE'] ? 'id="' . $row['PROPERTY_UF_ANCHOR_VALUE'] . '"' : '' ?>>
                <div class="section__side-bar side-bar side-bar_left">
                    <img src="<?= $row['PREVIEW_PICTURE'] ?>" class="side-bar__img">
                </div>
                <div class="section__content">
                    <div class="text-page">
                        <h3><?= $row['NAME'] ?></h3>
                        <?= $row['PREVIEW_TEXT'] ?>
                    </div>
                </div>
            </div>
        </div>
    <? else: ?>
        <div class="section section_little-pad section_color_white" <?= $row['PROPERTY_UF_ANCHOR_VALUE'] ? 'id="' . $row['PROPERTY_UF_ANCHOR_VALUE'] . '"' : '' ?>>
            <div class="section__inner section__inner_many-column container">
                <div class="section__content">
                    <div class="text-page">
                        <h3><?= $row['NAME'] ?></h3>
                        <?= $row['PREVIEW_TEXT'] ?>
                    </div>
                </div>
                <div class="section__side-bar side-bar side-bar_right">
                    <img src="<?= $row['PREVIEW_PICTURE'] ?>" class="side-bar__img">
                </div>
            </div>
        </div>
    <? endif; ?>
<? endforeach; ?>
