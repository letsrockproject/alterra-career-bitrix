<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

class PrincipleAndBenefits extends CBitrixComponent {
    public function onPrepareComponentParams($arParams) {
        $result = array("CACHE_TYPE" => $arParams["CACHE_TYPE"], "CACHE_TIME" => isset($arParams["CACHE_TIME"]) ? $arParams["CACHE_TIME"] : 36000000, "IBLOCK_ID" => intval($arParams["IBLOCK_ID"]));
        return $result;
    }

    public function executeComponent() {
        if ($this->startResultCache()) {
            CModule::IncludeModule("iblock");
            $arSelect = Array("ID", "NAME", "PREVIEW_TEXT", "PREVIEW_PICTURE", "PROPERTY_UF_ANCHOR");
            $arFilter = Array("IBLOCK_ID" => $this->arParams['IBLOCK_ID'], "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
            $res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter, false, Array("nPageSize" => 50), $arSelect);
            $arFields = [];

            while ($ob = $res->GetNextElement()) {
                $arFields[] = $ob->GetFields();
            }

            foreach ($arFields as $key => $row) {
                $arFields[$key]['PREVIEW_PICTURE'] = CFile::GetPath($row["PREVIEW_PICTURE"]);
                $date = new DateTime($row["PROPERTY_UF_YEAR_VALUE"]);
                $arFields[$key]['PROPERTY_UF_YEAR_VALUE'] = $date->format('Y');
            }

            $this->arResult = $arFields;
            $this->includeComponentTemplate();
        }

        return $this->arResult;
    }
} ?>