<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<div class="content-head">
    <div class="container container_no-pad">
        <div class="slider slider_header js-slider">
            <? foreach ($arResult as $row): ?>
                <div class="slide js-slide show">
                    <div class="slide__background">
                        <img src="<?= $row['PREVIEW_PICTURE'] ?>">
                    </div>
                    <div class="slide__wrap-text js-slide-title">
                        <h3 class="slide__title"><?= $row['NAME'] ?></h3>
                        <? if ($row['PROPERTY_UF_LINK_VALUE']): ?>
                            <p class="slide__text"><?= $row['PREVIEW_TEXT'] ?></p>
                        <? endif; ?>
                        <? if ($row['PROPERTY_UF_LINK_VALUE'] && $row['PROPERTY_UF_BUTTON_VALUE']): ?>
                            <a href="<?= $row['PROPERTY_UF_LINK_VALUE'] ?>" class="btn btn_color_red"><?= $row['PROPERTY_UF_BUTTON_VALUE'] ?></a>
                        <? endif; ?>
                    </div>
                </div>
            <? endforeach; ?>
            <div class="slider__controls">
                <a href="javascript:void(0);" class="slider__arrow prev js-slider-prev"></a>
                <a href="javascript:void(0);" class="slider__arrow next js-slider-next"></a>
            </div>
        </div>
    </div>
</div>
