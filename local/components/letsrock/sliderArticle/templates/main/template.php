<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<div class="section section_color_none">
    <div class="container container_no-pad article">
        <h3 class="article__table-title">Полезные статьи</h3>
        <div class="article__inner js-article-slider">
            <? foreach ($arResult as $row): ?>
                <div class="article__cart js-article-slide">
                    <div class="article__image">
                        <img src="<?= $row['PREVIEW_PICTURE'] ?>">
                    </div>
                    <div class="article__text-wrap">
                        <h4 class="article__title"><?= $row['NAME'] ?></h4>
                        <span class="article__text"><?= $row['PREVIEW_TEXT'] ?></span>
                        <a href="<?= $row['PROPERTY_UF_LINK_VALUE'] ?>" class="btn btn_color_blue article__btn btn_font_light">Подробнее</a>
                    </div>
                </div>
            <? endforeach; ?>
            <div class="article__slider-controls">
                <a href="javascript:void(0);"
                   class="article__slider-arrow article__slider-arrow_prev article__slider-arrow_disabled js-slider-prev"></a>
                <div class="article__numbers-wrap">
                    <span class="article__slider-number js-slider-number">1</span>
                    <span class="article__slider-quantity js-slider-quantity">3</span>
                </div>
                <a href="javascript:void(0);"
                   class="article__slider-arrow article__slider-arrow_next js-slider-next"></a>
            </div>
        </div>
    </div>
</div>