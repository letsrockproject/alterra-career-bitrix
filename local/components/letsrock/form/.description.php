<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$arComponentDescription = array(
    "NAME" => 'Отправка формы',
    "DESCRIPTION" => 'Слайдер контента',
    "PATH" => array(
        "ID" => "dv_components",
        "CHILD" => array(
            "ID" => "slider",
            "NAME" => "Слайдер"
        )
    ),
);
?>