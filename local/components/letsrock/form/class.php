<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Letsrock\Lib\Models\Form;

Loader::includeModule('letsrock.lib');

class FormComponent extends CBitrixComponent {
    public function onPrepareComponentParams($arParams) {
        $result = array("CACHE_TYPE" => $arParams["CACHE_TYPE"], "CACHE_TIME" => isset($arParams["CACHE_TIME"]) ? $arParams["CACHE_TIME"] : 36000000,);
        return $result;
    }

    public function executeComponent() {
        if ($this->startResultCache()) {
            $form = new Form();
            $this->arResult = $form->getDataByCityId();

            if (!empty($_GET['job'])) {
                foreach ($this->arResult['LIST_JOBS'] as $key => $job) {
                    if ($job['ID'] == $_GET['job']) {
                        $this->arResult['LIST_JOBS'][$key]['SELECTED'] = true;
                    }
                }
            }

            $this->includeComponentTemplate();
        }

        return $this->arResult;
    }
} ?>