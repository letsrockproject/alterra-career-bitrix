<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>
<div class="section">
    <div class="container">
        <form class="form-block js-form" action="/local/ajax/form/formSend.php">
            <div class="form-block__inner">
                <div class="form-block__column form-block__column_size_half">
                    <div class="input-field form-block__input-field form-block__input-field_select form-block__input-field_position_row">
                        <span class="input-field__label-text">Город</span>
                        <select class="input-field__select js-select-city" data-theme="light" name="town">
                            <? foreach ($arResult['LIST_CITY'] as $item): ?>
                                <option value="<?= $item['ID'] ?>"><?= $item['UF_NAME'] ?></option>
                            <? endforeach; ?>
                        </select>
                    </div>
                    <? if ($arResult['LIST_JOBS']): ?>
                        <div class="input-field form-block__input-field form-block__input-field_select js-input">
                            <span class="input-field__label-text">Должность</span>
                            <select class="input-field__select js-select js-select-jobs" data-theme="withbg" name="job"
                                    data-placeholder="Вакансии">
                                <option></option>
                                <? foreach ($arResult['LIST_JOBS'] as $item): ?>
                                    <option value="<?= $item['ID'] ?>" <?= empty($item['SELECTED']) ? "" : 'selected' ?>><?= $item['NAME'] ?></option>
                                <? endforeach; ?>
                            </select>
                        </div>
                    <? endif; ?>
                    <h3 class="form-block__heading">Личная информация</h3>
                    <div class="input-field form-block__input-field">
                        <label class="input-field__label"><span
                                    class="input-field__label-text">Ваши Ф.И.О</span>
                            <input type="text" name="name" class="input-field__text"
                                   placeholder="Введите ваше имя">
                        </label>
                    </div>
                    <div class="input-field form-block__input-field form-block__input-field_select">
                        <span class="input-field__label-text">Год рождения</span>
                        <select class="input-field__select js-select" data-theme="withbg" name="year"
                                data-placeholder="Выберите год">
                            <option></option>
                            <? for ($i = 1940; $i < date('Y'); $i++) :?>
                                <option value="<?=$i?>"><?=$i?></option>
                            <? endfor; ?>
                        </select>
                    </div>
                    <div class="input-field input-field_radio form-block__input-field">
                        <span class="input-field__label-text">Пол</span>
                        <div class="input-field__radio-group">
                            <label class="input-field__radio">
                                <input type="radio" class="input-field__radio" name="sex" value="men" checked>
                                <span class="input-field__radio-text">Мужской</span>
                            </label>
                            <label class="input-field__radio input-field__radio_last">
                                <input type="radio" class="input-field__radio" name="sex" value="women">
                                <span class="input-field__radio-text">Женский</span>
                            </label>
                        </div>
                    </div>
                    <h3 class="form-block__heading">Контактная информация</h3>
                    <div class="input-field form-block__input-field">
                        <label class="input-field__label"><span
                                    class="input-field__label-text">Номер телефона</span>
                            <input type="text" name="tel" class="input-field__text js-phone"
                                   placeholder="+7 (___) ___ - __ - __">
                        </label>
                    </div>
                    <div class="input-field form-block__input-field">
                        <label class="input-field__label"><span
                                    class="input-field__label-text">Email</span>
                            <input type="text" name="email" class="input-field__text"
                                   placeholder="Введите email">
                        </label>
                    </div>
                    <h3 class="form-block__heading">Запись на собеседование</h3>
                    <p class="form-block__text">Запишитесь на собеседование онлайн! Выберите удобное время — мы
                        обязательно получим заявку и
                        пришлем подтверждение в СМС и на почту.</p>
                    <div class="input-field input-field_radio form-block__input-field">
                        <span class="input-field__label-text">Адрес</span>
                        <div class="input-field__radio-group input-field__radio-group_type_vertical js-review-place">
                            <? foreach ($arResult['LIST_REVIEW_PLACES'] as $key => $item): ?>
                                <label class="input-field__radio js-form-city">
                                    <input type="radio" class="input-field__radio" data-name="<?=$item['PROPERTY_UF_ADDRESS_VALUE']?>" data-address="<?=$item['PROPERTY_UF_ADDRESS_VALUE']?>"  data-coordinates="<?=$item['PROPERTY_UF_MAP_VALUE']?>" value="<?=$item['ID']?>" name="address" <?= ($key === 0) ? "checked" : ""  ?>>
                                    <span class="input-field__radio-text"><?=$item['NAME']?>, <?=$item['PROPERTY_UF_ADDRESS_VALUE']?></span>
                                </label>
                            <? endforeach; ?>
                        </div>
                    </div>
                    <div class="input-field input-field_size_small form-block__input-field">
                        <label class="input-field__label input-field__label_type_date"><span
                                    class="input-field__label-text">Дата</span>
                            <input type="text" name="date"
                                   class="input-field__text input-field__text_little js-date"
                                   placeholder="Дата">
                        </label>
                    </div>
                    <div class="input-field input-field_size_small form-block__input-field form-block__input-field_select">
                        <span class="input-field__label-text">Время</span>
                        <select class="input-field__select js-select" data-theme="withbg" name="time">
                            <option value="09:00" selected>9:00 - 9:30</option>
                            <option value="09:30">9:30 - 10:00</option>
                            <option value="10:00">10:00 - 10:30</option>
                            <option value="10:30">10:30 - 11:00</option>
                            <option value="11:00">11:00 - 11:30</option>
                            <option value="11:30">11:30 - 12:00</option>
                            <option value="12:00">12:00 - 12:30</option>
                            <option value="12:30">12:30 - 13:00</option>
                            <option value="13:00">13:00 - 13:30</option>
                            <option value="13:30">13:30 - 14:00</option>
                            <option value="14:00">14:00 - 14:30</option>
                            <option value="14:30">14:30 - 15:00</option>
                            <option value="15:00">15:00 - 15:30</option>
                            <option value="15:30">15:30 - 16:00</option>
                            <option value="16:00">16:00 - 16:30</option>
                            <option value="16:30">16:30 - 17:00</option>
                        </select>
                    </div>
                    <div class="map js-y-maps" id="map"></div>
                    <div class="input-field form-block__input-field form-block__nda-block">
                        <input type="checkbox" class="input-field__checkbox" name="nda" id="checknda" checked/>
                        <label for="checknda" class="form-block__nda">Согласен на обработку
                            <a href="/nda/" class="form-block__nda-link" target="_blank">персональных данных</a>
                        </label>
                    </div>
                    <input name="file" type="file" class="input-field__file js-file-input">
                    <button class="btn btn_color_yellow">Отправить заявку</button>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="modal" id="form-error-file">
    <div class="modal__inner">
        <button data-izimodal-close="" class="modal__close"></button>
        <h3 class="modal__title">Ошибка</h3>
        <p class="js-form-error">Текст</p>
        <button data-izimodal-close="" class="btn btn_color_red modal__btn">Хорошо</button>
    </div>
</div>

<div class="modal" id="form-success">
    <div class="modal__inner">
        <h3 class="modal__title">Спасибо</h3>
        <p class="js-form-error">Ваша заявка принята, наш менеджер свяжется с вами</p>
        <a href="/" class="btn btn_color_yellow modal__btn">Перейти на главную</a>
    </div>
</div>
