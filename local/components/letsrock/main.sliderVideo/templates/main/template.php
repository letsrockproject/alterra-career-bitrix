<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<div class="section section_gradient section_not-top-pad">
    <div class="section__inner container container_no-pad">
        <div class="slider slider_video js-slider-video">
            <? foreach ($arResult as $row): ?>
                <div class="slide js-slide show">
                    <div class="slide__inner">
                        <div class="slide__video js-video-player">
                            <video class="js-video" src="<?= $row['PROPERTY_UF_VIDEO_VALUE']['path'] ?>"></video>
                            <div class="play js-play-control"></div>
                            <div class="stop js-stop-control"></div>
                        </div>
                        <p class="slide__video-title js-slide-title">
                            <?= $row['PREVIEW_TEXT'] ?>
                        </p>
                    </div>
                </div>
            <? endforeach; ?>
        </div>
    </div>
</div>
