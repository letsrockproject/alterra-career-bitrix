<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<div class="section section_color_gradient-blue">
    <div class="section__inner container">
        <h2 class="section__title section__title_with-pad">«Альтерра». Сделай правильный выбор!</h2>
        <div class="cart-table cart-table_size_big cart-table_with-controls article js-article-slider">
            <div class="article__slider-controls">
                <a href="javascript:void(0);"
                   class="article__slider-arrow article__slider-arrow_prev article__slider-arrow_disabled js-slider-prev"></a>
                <div class="article__numbers-wrap">
                    <span class="article__slider-number js-slider-number">1</span>
                    <span class="article__slider-quantity js-slider-quantity">3</span>
                </div>
                <a href="javascript:void(0);"
                   class="article__slider-arrow article__slider-arrow_next js-slider-next"></a>
            </div>
            <? foreach ($arResult as $row): ?>
                <div class="cart cart_size_big-v js-article-slide">
                    <div class="cart__image">
                        <img src="<?= $row['PREVIEW_PICTURE'] ?>">
                    </div>
                    <div class="cart__text-block cart__text-block_size_full">
                        <h4 class="cart__title"><?= $row['NAME'] ?></h4>
                        <p class="cart__text"><?= $row['PREVIEW_TEXT'] ?></p>
                        <a class="btn btn_color_invert-orange cart__btn" href="<?= $row['PROPERTY_UF_LINK_VALUE'] ?>">Подробнее</a>
                    </div>
                </div>
            <? endforeach; ?>
        </div>
    </div>
</div>
