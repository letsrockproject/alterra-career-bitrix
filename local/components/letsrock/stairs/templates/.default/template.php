<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<div class="stairs">
    <? foreach ($arResult as $row): ?>
        <div class="stairs__item">
            <div class="stairs__image"><img src="<?= $row['PREVIEW_PICTURE'] ?>"></div>
            <div class="stairs__text"><?= $row['NAME'] ?></div>
        </div>
    <? endforeach; ?>
</div>