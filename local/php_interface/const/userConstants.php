<?php
/**
 * Пользовательские константы
 */

const ASSETS_HOME = '/local/templates/assets/';

/**
 * Highload блоки
 */

const HL_CITY = 3;

/**
 * Инфоблоки
 */

const IB_JOB = 5;
const IB_STORE = 7;
const IB_RESUME = 8;
const IB_STAIRS = 10;
const IB_PRINCIPLE = 12;
const IB_YEAR_SLIDER = 11;
const IB_BENEFITS = 13;