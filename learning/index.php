<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Обучение в Альтерре"); ?>
    <p>Компания развивается с развитием каждого сотрудника. Это внутренняя синергия, без которой Альтерры бы
        не
        существовало. </p>
    <p>Мы с полной уверенностью можем сказать, что людям, которые стремятся отсидеться в компании, будет
        неуютно. Большое количество проектов и высокий темп работы, не позволят Вам остаться незамеченным.
        Зато
        мы готовы предложить Вам нетривиальные профессиональные задачи, благодаря которым Вы сможете
        развивать
        свои навыки и вряд ли соскучитесь. </p>
    <p>Система ценностей Компании «Альтерра» направлена на развитие способностей сотрудников обучаться,
        прогнозировать потребности, создавать новые источники конкурентного преимущества. Именно поэтому
        каждому
        сотруднику предоставляются возможности для обучения и развития. </p>
    <img width="1024" alt="Rectangle (2).jpg"
         src="/upload/medialibrary/f2d/f2d6f9308d5d460088496d9c22cc5eef.jpg" height="381"
         title="Rectangle (2).jpg" class="section__banner">
    <p>Обучением и развитием сотрудника занимается его непосредственный руководитель, а также менеджер по
        развитию персонала. </p>
    <p>Мы стремимся сделать непрерывное развитие каждого сотрудника неотъемлемой частью корпоративной
        культуры
        Компании, в которой знания рассматриваются как главный стратегический ресурс успеха.</p>
    <p>Сотрудники Компании «Альтерра» имеют отличную возможность повышать квалификацию уже с первых дней
        работы
        в компании. В процессе обучения новые сотрудники знакомятся с преимуществами и особенностями
        компании
        Альтерра , правилами клиентоориентированности, устройством и структурой наших торговых комплексов,
        многочисленным ассортиментом. Помимо этого знакомятся с функциями своего отдела и уже делают первые
        шаги
        в их выполнении. </p>
    <img width="1024" alt="Rectangle (1).jpg"
         src="/upload/medialibrary/345/3453efa7b406a229f5b81f5d799fa5e6.jpg" height="381"
         title="Rectangle (1).jpg" class="section__banner">
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>