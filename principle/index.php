<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Принципы работы в команде Компании «Альтерра»"); ?><div class="section section_little-pad">
	<div class="section__inner container">
		<div class="text-page">
			<p>
				 Исторически сложилось, что «Альтерра» начиналась с ряда основополагающих принципов, которые по сей день актуальны для каждого сотрудника и позволяют формировать лицо Компании на внешнем рынке и дружественную плодотворную атмосферу внутри.
			</p>
			<p>
				 Мы не любим громких лозунгов. То, что для нас по-настоящему важно, сформулировано в данном документе. Наши принципы написаны улыбками довольных сотрудников и эффективной работой.
			</p>
		</div>
	</div>
</div>
 <?$APPLICATION->IncludeComponent(
	"letsrock:principleAndBenefits",
	"",
	Array(
		"IBLOCK_ID" => "12",
		"SECTION_ID" => "12"
	)
);?>
<div class="section section_little-pad">
	<div class="section__inner container">
		<div class="text-page">
			<p>
				 Относитесь с вниманием к тем, кто работает с Вами рядом. Поддержите их, когда им это необходимо. Рассчитывайте на поддержку и поддержку с их стороны. Альтерра – это одна команда и это команда друзей, людей близких по духу, интересам , жизненным принципам и интеллектуальному уровню. От индивидуального успеха каждого участника команды, зависит успех всех. Не бойтесь обратиться к другим за помощью, знаниями, поддержкой и советом.
			</p>
		</div>
	</div>
</div>
<br><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>