<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("В «Альтерре» здорово!"); ?><div class="section">
	<div class="section__inner container">
		<div class="text-page">
			<p>
				Ты находишься в поисках своей первой работы? Приходи в «Альтерру». Ты сможешь не только зарабатывать, но и получишь интересные задачи, возможности для саморазвития и реализации своих идей. Ты можешь начать карьеру там, где тебе больше нравится: стать сотрудником Торгово-складского комплекса или специалистом офиса.
			</p>
			<p>
				Мы гарантируем тебе новые знакомства и профессиональный опыт, который поможет не только расти по карьерной лестнице, но и иметь действительно большие профессиональные преимущества.
			</p>
			<p>
				В «Альтерре» можно научиться не просто работать, а делать это с удовольствием!
			</p>
		</div>
 <img alt="VNV_3703-2.jpg" src="/upload/medialibrary/993/99322d15ea641a24200a446684e7c9df.jpg" title="VNV_3703-2.jpg" class="section__banner">
		<div class="text-page">
			<h3>Мы предлагаем</h3>
			<p>
				Мы обеспечиваем все условия для успешной работы и карьеры. Ты будешь работать в команде энергичных профессионалов. А твой руководитель и наставник всегда помогут опытным советом или решением сложных ситуаций, возникающих в процессе работы.
			</p>
			<h3>Как сделать карьеру в «Альтерре»? </h3>
			<p>
				Ты знал, что, согласно статистике, около 90% всех руководителей разных компаний начинали свою карьеру простыми продавцами? «Альтерра» предоставляет все возможности для карьерного роста тем, кто хочет развиваться, учиться и однажды стать успешным руководителем. Все зависит от тебя ! «Альтерра» ценит своих сотрудников и старается предоставить им все возможности для карьерного роста. Поэтому, когда в компании открывается вакансия руководителя, то в первую очередь рассматриваются уже работающие сотрудники с необходимыми навыками, которые могут стать успешными руководителями и применять весь свой имеющийся опыт.
			</p>
 <a href="/request/" class="btn btn_margined btn_color_yellow">Отправить заявку</a> <a href="/jobs/" class="btn btn_margined btn_color_red">Посмотреть все вакансии</a>
		</div>
		 <?$APPLICATION->IncludeComponent(
	"letsrock:stairs",
	"",
Array()
);?>
	</div>
</div><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>